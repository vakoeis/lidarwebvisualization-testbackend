// sended prerecorded data

const Stream = require('stream')

const data = require("./sick_files/daterrrg_module").data;

const app = require('express')();
const http = require('http').Server(app);
const socket = require('socket.io')(http);


app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});

const sleep = time => new Promise(res => setTimeout(() => res(), time));

// console.log(data[0].length)
// socket.on("connection", async socket => {
// 	console.log("connected\n\t")
// socket.emit("server message", {
// 		time: +new Date(),
// 		message: "Hiiiiii, Pong"
// 	});


// 	socket.on("client message", async msg => {
// 		console.log("client " + msg.name + " message: " + msg.message);
// 		socket.emit("server message", {
// 			time: +new Date(),
// 			message: "Pong"
// 		});
// 	});

// });


// emulate getting data

const lidarSpeedHerz = 15;

const dataStream = new Stream.Readable({
	read() {}
});

let i = 0;
const interval = setInterval(() => {
	i %= data.length;
	const arr = new Uint8Array(12976);
	data[i++].forEach((el, i) => arr[i] = el);
	// console.debug("\ngot new data")
	dataStream.push(arr);
}, 1000/lidarSpeedHerz);


let peers = 0;

socket.on("connection", async socket => {
	peers++;
	const ip = socket.request.connection.remoteAddress;
	const userAgent = socket.request.headers['user-agent'];
	console.log("\nconnected", ip,"("+peers+" connections)","\n\t User Agent:",userAgent,"\n\t Socket ID:", socket.id)

	const myStream = new Stream.PassThrough();
	dataStream.pipe(myStream);

	const listener = () => {
		// console.log("\n",socket.id + " listened")
		const frame = myStream.read();

		if (!frame) throw new Error("frame was null", frame);

		// console.debug("sending data to", socket.id, "("+i+")");

		socket.emit("frame", {
			time: +new Date(),
			message: frame,
			id: i // debug
		});
	}

	myStream.on('readable', listener);
	socket.on('disconnect', function(){
		peers--;
		const ip = socket.request.connection.remoteAddress;
		const userAgent = socket.request.headers['user-agent'];
		console.log("\ndisconnected", ip, "("+peers+" connections)", "\n\t", ip,"\n\t",userAgent,"\n\t", socket.id)
		// myStream.off('readable', listener);
	});
});

