const Peer = require('simple-peer')
const wrtc = require('wrtc')

const httpServer = require('http').createServer();
const io = require('socket.io')(httpServer);

const Stream = require('stream')


// const socket = require('socket.io')(http);
// const p2p = require('socket.io-p2p-server').Server;
// socket.use(p2p); // use webrtc


const fs = require('fs');
const fifoPath = '/tmp/nodeFIFO';


// TOOLS

const sleep = time => new Promise(res => setTimeout(() => res(), time));

function jsonSummary(json) {
	return getSummary(JSON.stringify(json));
}

/** returns first and last chars of a string */
function getSummary(s) {
	if (s.length <= 100) return s;
	return s.substring(0, 50) + "…" + s.substring(s.length - 50, s.length);
}

// webserver for socket.io (idk why you need this but ok)
const port = process.env.PORT || 3000;
httpServer.listen(port);

// named pipe anzapfen
const fifo = fs.createReadStream(fifoPath);
fifo.setEncoding("utf8");

console.log("started listener...");


// create stream providing complete data packets
// not the bits and pieces returned from the fifo
const packetStream = new Stream.Readable({
	read() {}
});



let buffer = ""; // buffers incoming data
let frameNo = 0; // debug
const packetDelimiter = "EOT"; // string that separates packets

const sendAllPackets = false;

// buffer the FIFO stream, collect pieces of data, and turn into packet stream
fifo.on("data", msg => {

	buffer += msg;

	let packet;

	if (sendAllPackets) {
		// there could potentially be multiple packets in the buffer
		// so loop until we pushed all of them to the packet stream
		for (let delIndex = buffer.indexOf(packetDelimiter);
				delIndex !== -1;
				delIndex = buffer.indexOf(packetDelimiter)) {

			packet = buffer
				.substring(0, delIndex)
				.replace(/\0/g, ''); // dis shit too null terminated boi

			buffer = buffer.substring(delIndex + packetDelimiter.length, buffer.length);

		}
	} else {
		if (!~buffer.indexOf(packetDelimiter)) return;
		const split = buffer.split(packetDelimiter);
		// keep the beginning of the freshest packet (might be empty)
		// throw everything else away
		buffer = split[split.length - 1];
		// send the freshest packet
		packet = split[split.length - 2].replace(/\0/g, '');
	}

	packetStream.push(packet);
	frameNo++; // debug
});

let peers = 0;

function logAction(action, buddy) {
	// const ip = socket.request.connection.remoteAddress;
	// const userAgent = socket.request.headers['user-agent'];
	console.log(
		"\n"+action, /*ip, */"(" + peers + " connections)",
		/*"\n\t User Agent:", userAgent, */"\n\t buddy ID:", buddy.id
	);
}


function initiateSendingData(peer) {
	console.log("now able to send data to", peer.id)

	peers++;
	logAction("connected", peer);

	// packet stream anzapfen
	const myStream = new Stream.PassThrough();
	myStream.setEncoding('utf8');
	packetStream.pipe(myStream);

	myStream.on('readable', () => {
		// console.log("\n",socket.id + " listened")
		let rawData = myStream.read();
		if (!rawData) throw new Error("rawData was " + rawData);

		let data;
		try {
			data = JSON.parse(rawData);
		} catch (e) {
			if (rawData) {
				console.error(
					"couldn't parse JSON of length ", rawData.length,
					"\t`"+getSummary(rawData)+"`"
				);
			} else {
				console.error("rawData was", rawData);
			}
			console.error(e)
			console.log(rawData);
			throw e;
			return;
		}
		// send the packet to the visualization through socket.io
		// console.log("sending", getSummary(packet))
		peer.send(JSON.stringify({
			frame: data.frame,
			clusters: data.clusters,
			id: frameNo, // debug
			timestamps: {
				sent: +new Date(),
				birth: data.birth,
			},
		}));
	});
}


// connection stuff

io.on('connection', clientSocket => {
	const id = clientSocket.id; // 'G5p5...'
	console.log("("+id+")", 'a user connected');

	clientSocket.on('disconnect', () => {
		console.log("("+id+")", 'a user disconnected');
	});

	const serverPeer = new Peer({
		wrtc,
		objectMode: true,
		trickle: false,
		iceTransportPolicy: 'relay',
		reconnectTimer: 3000,
		config: {
			"iceServers": [
			{
				urls: "stun:eu-turn4.xirsys.com",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turn:eu-turn4.xirsys.com:80?transport=udp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turn:eu-turn4.xirsys.com:3478?transport=udp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turn:eu-turn4.xirsys.com:80?transport=tcp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turn:eu-turn4.xirsys.com:3478?transport=tcp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turns:eu-turn4.xirsys.com:443?transport=tcp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}, {
				urls: "turns:eu-turn4.xirsys.com:5349?transport=tcp",
					"username": "UlBvoMNYBgIumfXF34CGim4bUMO7kZdaOxfM3pk1VrV7A_JP2s47tyVNyPYDGlYOAAAAAFzjFqhpanVzdHdhbnRhc2VydmVy",
					"credential": "0863149a-7b43-11e9-9435-6adcafebbb45"
				}
			],
		},
	});
	// create a unique peer instance for each peer, not one for all

	// when serverPeer has signaling data, send it to clientSocket
	serverPeer.on('signal', data => {
		console.log("("+id+")", "sending signaling data", jsonSummary(data));
		clientSocket.emit("signaling_message", data); // -> peer1.signal(data)
	})

	// receive the clientSocket's signaling messages
	clientSocket.on('signaling_message', data => {
		console.log("("+id+")", "received signaling data", jsonSummary(data));
		serverPeer.signal(data);
	});


	serverPeer.on('error', err => {
		console.error("("+id+")", err)
	})

	serverPeer.on("connect", () => {
		logAction("connected", clientSocket);
		// wait for "connect" event before using the data channel
		// serverPeer.send("hey clientSocket, how is it going?")
		initiateSendingData(serverPeer);
	})

	serverPeer.on('data', data => {
		// got a data channel message
		console.log("("+id+")", 'got a message from clientSocket: ' + data);
	})
	serverPeer.on('close', data => {
		// got a data channel message
		console.log("("+id+")", 'clientSocket disconnected');
		peers--;
		logAction("closed", clientSocket);
	})
});
