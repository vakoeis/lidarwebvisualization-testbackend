# lidarViz Testing Backend

Sends dummy data to lidarViz.

Includes the

- dummy point-cloud-data sender `data_generator.cpp`
- Node process which sends data to the visualization via WebRTC `index.js`

## Setup

1. install Node and npm (use nvm, the Node version manager)
2. `npm install`

## Run

0. Start peerjs server in the peerjs-server directory
1. Start the dummy point cloud data sender `g++ data_generator.cpp -fpermissive -o main && ./main`
2. Start the Node process which sends Data to the visualization via WebRTC `node index.js`
3. Start the visualization
